


// S28 Activity Template:


// 1.) Insert a single room using the updateOne() method.
// Code here:

db.hotel.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"roomsAvailable": 10,
	"isAvailable": false
});


// 2.) Insert multiple rooms using the insertMany() method.
// Code here:

db.hotel.insertMany([
	{
	"name": "double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small family going on a vacation",
	"roomsAvailable": 5,
	"isAvailable": false		
	},
	{
	"name": "queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"roomsAvailable": 15,
	"isAvailable": false		
	}
]);

// 3.) Use find() method to search for a room with a name "double".
// Code here:
db.hotel.find({
	"name": "double"
});


// 4.) Use the updateOne() method to update the queen room and set the available rooms to 0.
// Code here:

db.hotel.updateOne(
	{
		"name": "queen"
	},
	{
		$set: {
			"roomsAvailable": 0
		}
	});

// 5.) Use the deleteMany method to delete all rooms that have 0 rooms avaialable.
// Code here:

db.hotel.deleteMany({
	"roomsAvailable": 0
});