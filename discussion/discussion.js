// CRUD Operations:
/*
	 1. Create - insert
	 2. Read - find method
	 3. Update - update
	 4. Destroy - delete
*/

// Insert Method () - Create documents in our database.

/*
	Syntax:
		Insert One Document:
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB",
			});

		Insert Many Documents:
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB",	
				}
			]);
*/

// upon execution on Robo 3T make sure not to execute your data several times as it will duplicate

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Dela Cruz",
	"age": 21,
	"email": "janedc@ymail.com",
	"company": "none"
});

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawkings",
			"age": 67,
			"email": "stephenhawkings@gmail.com",
			"department": "none"
		},
		{
			"firstName": "Niel",
			"lastName": "Armstrong",
			"age": 82,
			"email": "nielarmstong@gmail.com",
			"department": "none"
		},		
	]);

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": true
	}
	]);

// Find Document/Method - Read 

/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our DB.

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria.

		db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that match our criteria.

		db.collectionName.findOne({}) - This will return the frst document in our collection.
*/

db.users.find();